package atr.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AtrDTO {

	public AtrDTO(Long id, Long accountId, Long teamId, Long roleId) {
		super();
		this.id = id;
		this.accountId = accountId;
		this.teamId = teamId;
		this.roleId = roleId;
	}

	private Long id;
	private Long accountId;
	private Long teamId;
	private Long roleId;

	public AtrDTO() {

	}
}