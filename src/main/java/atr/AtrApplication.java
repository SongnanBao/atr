package atr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AtrApplication {

	public static void main(String[] args) {
		SpringApplication.run(AtrApplication.class, args);
	}
}
