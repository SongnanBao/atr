package atr.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import atr.model.ATR;
import atr.model.Account;
import atr.model.Role;
import atr.model.Team;
import atr.repository.RoleRepository;

@Service
public class RoleService {

	@Autowired
	private RoleRepository roleRepo;

	@Transactional
	public Role save(Role role) {
		return roleRepo.save(role);
	}

	@Transactional
	public Role create(String name, String description) {
		return roleRepo.save(new Role(name, description));
	}

	@Transactional
	public String delete(long id) {
		Role role = roleRepo.getOne(id);
		if (role == null) {
			return null;
		}
		String name = role.getName();
		try {
			roleRepo.delete(id);
		} catch (Exception ex) {
			return "Error deleting the role:" + ex.toString();
		}
		return "Role '" + name + "' was succesfully deleted!";
	}

	public Role get(long id) {
		return roleRepo.findOne(id);
	}

	public Role getByName(String name) {
		return roleRepo.findByName(name);
	}

	@Transactional
	public Role update(Role role) {
		return roleRepo.save(role);
	}

	@SuppressWarnings({
		"rawtypes", "unchecked"
	})
	public List<Team> getTeams(long id) {
		Set<Team> teams = new HashSet<Team>();
		Role role = roleRepo.getOne(id);
		if (role == null) {
			return null;
		}
		List<ATR> atrs = role.getAtrs();
		for (ATR atr : atrs) {
			teams.add(atr.getTeam());
		}
		return new ArrayList(teams);
	}

	@SuppressWarnings({
		"rawtypes", "unchecked"
	})
	public List<Account> getAccounts(long id) {
		Set<Account> accounts = new HashSet<Account>();
		Role role = roleRepo.getOne(id);
		if (role == null) {
			return null;
		}
		List<ATR> atrs = role.getAtrs();
		for (ATR atr : atrs) {
			accounts.add(atr.getAccount());
		}
		return new ArrayList(accounts);
	}

	public List<Role> getAll() {
		return roleRepo.findAll();
	}
}
