package atr.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import atr.exception.RestfulException;
import atr.model.ATR;
import atr.model.Account;
import atr.model.Role;
import atr.model.Team;
import atr.repository.TeamRepository;

@Service
public class TeamService {

	@Autowired
	private TeamRepository teamRepo;

	@Transactional
	public Team save(Team team) {
		return teamRepo.save(team);
	}

	@Transactional
	public Team create(String name, String description) {
		return teamRepo.save(new Team(name, description));
	}

	@Transactional
	/*
	 * Set parent for a team Team arg1 must not have any parent or ancestor, and team arg2 must not be its descendant.
	 */
	public Team setParent(long id, long parentId) {
		if (parentId == id) {
			throw new RestfulException("Unable to set itself as parent.", HttpStatus.BAD_REQUEST.value());
		}
		Team team = teamRepo.getOne(id);
		if (team == null || team.isRemoved()) {
			throw new RestfulException("Team (ID:" + id + ") does not exist.", HttpStatus.BAD_REQUEST.value());
		}
		Team parent = teamRepo.findOne(parentId);
		if (parent == null || parent.isRemoved()) {
			throw new RestfulException("Team (ID:" + parentId + ") does not exist.", HttpStatus.BAD_REQUEST.value());
		}
		if (this.getParent(id) != null) {
			throw new RestfulException("Team (ID:" + id + ") already has parent Team.", HttpStatus.BAD_REQUEST.value());
		}
		if (this.isAncestor(id, parentId)) {
			throw new RestfulException("Unable to set descendant Team (ID:" + parentId + ") as parent of Team (ID:" + id + ").", HttpStatus.BAD_REQUEST.value());
		}
		team.setParent(parent);
		return teamRepo.save(team);
	}

	/*
	@Transactional
	public String delete(long id) {
		try {
			List<Team> children = getChildren(id);
			if (children!= null && children.size() > 0) {
				throw new RestfulException("Unable to delete a Team which has children teams.", HttpStatus.BAD_REQUEST.value());
			}
			teamRepo.delete(id);
		} catch (Exception ex) {
			return "Error deleting the team:" + ex.toString();
		}
		return "Team succesfully deleted!";
	}
	 */

	@Transactional
	public String delete(long id) {
		try {
			List<Team> children = getChildren(id);
			if (children!= null && children.size() > 0) {
				throw new RestfulException("Unable to delete a Team which has children teams.", HttpStatus.BAD_REQUEST.value());
			}
			Team team = teamRepo.getOne(id);
			team.setRemoved(true);
			team.setDisabled(true);
			teamRepo.save(team);
		} catch (Exception ex) {
			return "Error when deleting the team:" + ex.toString();
		}
		return "Team was succesfully deleted!";
	}

	@Transactional
	public String restore(long id) {
		try {
			Team team = teamRepo.getOne(id);
			team.setRemoved(false);
			teamRepo.save(team);
		} catch (Exception ex) {
			return "Error when restoring the team:" + ex.toString();
		}
		return "Team was succesfully restored!";
	}

	@Transactional
	public String disable(long id) {
		try {
			List<Team> children = getChildren(id);
			if (children!= null && children.size() > 0) {
				throw new RestfulException("Unable to disable a Team which has children teams.", HttpStatus.BAD_REQUEST.value());
			}
			Team team = teamRepo.getOne(id);
			team.setDisabled(true);
			teamRepo.save(team);
		} catch (Exception ex) {
			return "Error when disabling the team:" + ex.toString();
		}
		return "Team was succesfully disabled!";
	}

	@Transactional
	public String enable(long id) {
		try {
			Team team = teamRepo.getOne(id);
			team.setDisabled(false);
			teamRepo.save(team);
		} catch (Exception ex) {
			return "Error when enabling the team:" + ex.toString();
		}
		return "Team was succesfully enabled!";
	}

	public Team get(long id) {
		return teamRepo.findOne(id);
	}

	public List<Team> getByName(String name) {
		return teamRepo.findByName(name);
	}

	@Transactional
	public Team update(Team team) {
		return teamRepo.save(team);
	}

	public Team getParent(long id) {
		Team team = teamRepo.findOne(id);
		if (team == null) {
			return null;
		}
		return team.getParent();
	}

	public List<Team> getChildren(long id) {
		Team team = teamRepo.findOne(id);
		if (team == null) {
			return null;
		}
		return team.getChildren();
	}

	@Transactional
	public List<Account> getAccounts(long id) {
		List<Account> accounts = new ArrayList<Account>();
		Team team = teamRepo.getOne(id);
		if (team == null) {
			return null;
		}
		List<ATR> atrs = team.getAtrs();
		for (ATR atr : atrs) {
			Account account = atr.getAccount();
			if (! accounts.contains(account)) {
				accounts.add(account);
			}
		}
		return accounts;
	}

	@Transactional
	public List<Role> getRoles(long id) {
		List<Role> roles = new ArrayList<Role>();
		Team team = teamRepo.getOne(id);
		if (team == null) {
			return null;
		}
		List<ATR> atrs = team.getAtrs();
		for (ATR atr : atrs) {
			Role role = atr.getRole();
			if (! roles.contains(role)) {
				roles.add(role);
			}
		}
		return roles;
	}

	public List<Team> getAll() {
		return teamRepo.findAll();
	}

	@Transactional
	public String removeParent(long id, long parentId) {
		try {
			Team team = teamRepo.getOne(id);
			if (team.getParent() == null) {
				throw new Exception("Team (ID:" + id + ") does not have parent.");
			}
			team.setParent(null);
			teamRepo.save(team);
		} catch (Exception ex) {
			return "Error removing parent Team. " + ex.toString();
		}
		return "Parent Team was succesfully removed.";
	}

	@Transactional
	public List<Team> getDescendants(long id) {
		List<Team> descendants = new ArrayList<Team>();
		List<Team> teamStack = new ArrayList<Team>();
		teamStack.add(teamRepo.findOne(id));
		while (!teamStack.isEmpty()) {
			Team parent = teamStack.get(0);
			List<Team> children = parent.getChildren();
			if (children != null && !children.isEmpty()) {
				for (Team child : children) {
					descendants.add(child);
					teamStack.add(child);
				}
			}
			teamStack.remove(0);
		}
		return descendants;
	}

	public List<Long> getDescendantIds(long id) {
		List<Long> descendants = new ArrayList<Long>();
		List<Team> teamStack = new ArrayList<Team>();
		teamStack.add(teamRepo.findOne(id));
		while (!teamStack.isEmpty()) {
			Team parent = teamStack.get(0);
			List<Team> children = parent.getChildren();
			if (children != null && !children.isEmpty()) {
				for (Team child : children) {
					descendants.add(child.getId());
					teamStack.add(child);
				}
			}
			teamStack.remove(0);
		}
		return descendants;
	}

	public List<Team> getAncestors(long id) {
		List<Team> ancestors = new ArrayList<Team>();
		List<Team> teamStack = new ArrayList<Team>();
		teamStack.add(teamRepo.findOne(id));
		while (!teamStack.isEmpty()) {
			Team child = teamStack.get(0);
			Team parent = child.getParent();
			if (parent != null) {
				ancestors.add(parent);
				teamStack.add(parent);
			}
			teamStack.remove(0);
		}
		return ancestors;
	}

	public List<Long> getAncestorIds(long id) {
		List<Long> ancestors = new ArrayList<Long>();
		List<Team> teamStack = new ArrayList<Team>();
		teamStack.add(teamRepo.findOne(id));
		while (!teamStack.isEmpty()) {
			Team child = teamStack.get(0);
			Team parent = child.getParent();
			if (parent != null) {
				ancestors.add(parent.getId());
				teamStack.add(parent);
			}
			teamStack.remove(0);
		}
		return ancestors;
	}

	/*
	 * Check if team1 is descendant of team2. Returns true if team1 is descendant of team2.
	 * Better to use isAncestor(long teamId2, long teamId1) instead of this;
	 */
	public boolean isDescendant(long teamId1, long teamId2) {
		List<Team> teamStack = new ArrayList<Team>();
		teamStack.add(teamRepo.findOne(teamId2));
		while (!teamStack.isEmpty()) {
			Team parent = teamStack.get(0);
			List<Team> children = parent.getChildren();
			if (children != null && !children.isEmpty()) {
				for (Team child : children) {
					Long childId = child.getId();
					if (childId == teamId1) {
						return true;
					}
					teamStack.add(child);
				}
			}
			teamStack.remove(0);
		}
		return false;
	}

	/*
	 * Check if team1 is ancestor of team2 Return true if team1 is ancestor of team2.
	 * Recommend this method more than isDescendant() for better efficiency;
	 */
	public boolean isAncestor(long teamId1, long teamId2) {
		List<Team> teamStack = new ArrayList<Team>();
		teamStack.add(teamRepo.findOne(teamId2));
		while (!teamStack.isEmpty()) {
			Team child = teamStack.get(0);
			Team parent = child.getParent();
			if (parent != null) {
				if (parent.getId() == teamId1) {
					return true;
				}
				teamStack.add(parent);
			}
			teamStack.remove(0);
		}
		return false;
	}

}
