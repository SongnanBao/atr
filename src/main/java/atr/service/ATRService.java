package atr.service;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import atr.model.ATR;
import atr.repository.ATRRepository;

@Service
@Transactional
public class ATRService {

	public static final Logger LOG = LogManager.getLogger(ATRService.class);
	private static final String ERROR_FORMAT = "Error occures when %s the Account&Team&Role. ";

	@Autowired
	private ATRRepository atrRepo;
	@Autowired
	private AccountService accountService;
	@Autowired
	private TeamService teamService;
	@Autowired
	private RoleService roleService;

	public ATR save(ATR atr) {
		return atrRepo.save(atr);
	}

	public ATR createAtr(long accountId, long teamId, long roleId) {
		try {
			return atrRepo.save(new ATR(accountService.get(accountId), teamService.get(teamId), roleService.get(roleId)));
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return null;
	}

	public ATR getAtr(long id) {
		return atrRepo.getOne(id);
	}

	public ATR getAtr(Long accountId, Long teamId, Long roleId) throws Exception {
		return atrRepo.findByAccountIdAndTeamIdAndRoleId(accountId, teamId, roleId);
	}

	public List<ATR> getAllAtrDTOs() {
		return atrRepo.findAll();
	}

	public List<ATR> getAtrsByAccountAndTeam(long accountId, long teamId) throws Exception {
		return atrRepo.findByAccountIdAndTeamId(accountId, teamId);
	}

	public List<ATR> getAtrsByAccountAndRole(long accountId, long roleId) throws Exception {
		return atrRepo.findByAccountIdAndRoleId(accountId, roleId);
	}

	public List<ATR> getAtrsByTeamAndRole(long teamId, long roleId) throws Exception {
		return atrRepo.findByTeamIdAndRoleId(teamId, roleId);
	}

	public List<ATR> getAtrsByAccount(long accountId) throws Exception {
		return atrRepo.findByAccountId(accountId);
	}

	public List<ATR> getAtrsByTeam(long teamId) throws Exception {
		return atrRepo.findByTeamId(teamId);
	}

	public List<ATR> getAtrsByRole(long roleId) throws Exception {
		return atrRepo.findByRoleId(roleId);
	}

	public void removeAtr(long id) throws Exception {
		atrRepo.delete(id);
	}

	public void removeAtr(long accountId, long teamId, long roleId) throws Exception {
		ATR atr = atrRepo.findByAccountIdAndTeamIdAndRoleId(accountId, teamId, roleId);
		if (atr != null) {
			atrRepo.delete(atr);
		} else {
			throw new Exception("Unable to find existing ATR record (accountId: " + accountId + ", teamId: " + teamId + ", roleId: " + roleId + ")");
		}
	}

	public void removeAtrsByAccount(long accountId) throws Exception {
		try {
			atrRepo.deleteByAccountId(accountId);
		} catch (Exception ex) {
			throw new Exception(String.format(ERROR_FORMAT, "removing") + ex.getMessage());
		}
	}

	public void removeAtrsByTeam(long teamId) throws Exception {
		try {
			atrRepo.deleteByTeamId(teamId);
		} catch (Exception ex) {
			throw new Exception(String.format(ERROR_FORMAT, "removing") + ex.getMessage());
		}
	}

	public void removeAtrsByRole(long roleId) throws Exception {
		try {
			atrRepo.deleteByRoleId(roleId);
		} catch (Exception ex) {
			throw new Exception(String.format(ERROR_FORMAT, "removing") + ex.getMessage());
		}
	}

	public void removeAtrsByAccountAndTeam(long accountId, long teamId) throws Exception {
		try {
			atrRepo.deleteByAccountIdAndTeamId(accountId, teamId);
		} catch (Exception ex) {
			throw new Exception(String.format(ERROR_FORMAT, "removing") + ex.getMessage());
		}
	}

	public void removeAtrsByAccountAndRole(long accountId, long roleId) throws Exception {
		try {
			atrRepo.deleteByAccountIdAndRoleId(accountId, roleId);
		} catch (Exception ex) {
			throw new Exception(String.format(ERROR_FORMAT, "removing") + ex.getMessage());
		}
	}

	public void removeAtrsByTeamAndRole(long teamId, long roleId) throws Exception {
		try {
			atrRepo.deleteByTeamIdAndRoleId(teamId, roleId);
		} catch (Exception ex) {
			throw new Exception(String.format(ERROR_FORMAT, "removing") + ex.getMessage());
		}
	}
	/*
	public List<AtrDTO> atrsToAtrDTOs(List<ATR> atrs) {
		List<AtrDTO> atrDTOs = new ArrayList<AtrDTO>();
		for (ATR atr : atrs) {
			atrDTOs.add(atr.toDTO());
		}
		return atrDTOs;
	}*/
}
