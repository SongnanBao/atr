package atr.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import atr.model.ATR;
import atr.model.Account;
import atr.model.Role;
import atr.model.Team;
import atr.repository.AccountRepository;

@Service
@Transactional
public class AccountService {

	public static final Logger LOG = LogManager.getLogger(AccountService.class);

	@Autowired
	private AccountRepository repo;

	public Account save(Account account) {
		return repo.save(account);
	}

	public Account create(String email, String firstname, String lastname, String password) {
		Account account = new Account(email, firstname, lastname, password);
		return repo.save(account);
	}
	/*
	 *
	 * public String delete(long id) { try { accountRepo.delete(id); } catch (Exception ex) { return "Error when deleting the account:" +
	 * ex.toString(); } return "Account was succesfully deleted!"; }
	 */

	public void disable(long id) {
		try {
			Account account = repo.findOne(id);
			account.setDisabled(true);
			repo.save(account);
		} catch (Exception ex) {
			LOG.error(ex.getMessage());
		}
	}

	public void enable(long id) {
		try {
			Account account = repo.findOne(id);
			account.setDisabled(false);
			repo.save(account);
		} catch (Exception ex) {
			LOG.error(ex.getMessage());
		}
	}

	public Account get(long id) {
		return repo.findOne(id);
	}

	public Account getByEmail(String email) {
		return repo.findByEmail(email);
	}

	@SuppressWarnings({
		"unchecked", "rawtypes"
	})
	public List<Team> getTeams(long id) {
		Set<Team> teams = new HashSet<Team>();
		Account account = repo.getOne(id);
		if (account == null) {
			return null;
		}
		List<ATR> atrs = account.getAtrs();
		for (ATR atr : atrs) {
			teams.add(atr.getTeam());
		}
		return new ArrayList(teams);
	}

	@SuppressWarnings({
		"unchecked", "rawtypes"
	})
	public List<Role> getRoles(long id) {
		Set<Role> roles = new HashSet<Role>();
		Account account = repo.getOne(id);
		if (account == null) {
			return null;
		}
		List<ATR> atrs = account.getAtrs();
		for (ATR atr : atrs) {
			roles.add(atr.getRole());
		}
		return new ArrayList(roles);
	}

	public List<Account> getAll() {
		return repo.findAll();
	}

	public List<Account> getAllByFirstname() {
		return repo.findAllByFirstname();
	}

	public List<Account> getAllByLastname() {
		return repo.findAllByLastname();
	}

	public void removeAccount(long id) {
		repo.delete(id);
	}

	public void removeAccountByEmail(String email) {
		repo.deleteByEmail(email);
	}
}
