package atr.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;

import atr.util.json.Views;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "t_account", uniqueConstraints = @UniqueConstraint(columnNames = {
		"id", "email"
}))
@DynamicUpdate(value=true)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Account {

	public Account() {

	}

	public Account(String email, String firstname, String lastname, String password) {
		super();
		this.email = email;
		this.firstname = firstname;
		this.lastname = lastname;
		this.password = password;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonView(Views.BasicView.class)
	private Long id;

	@NotNull
	@Column(unique = true, length = 63)
	@JsonView(Views.BasicView.class)
	private String email;

	@OneToMany(mappedBy = "account", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonIgnore
	private List<ATR> atrs = new ArrayList<ATR>();

	@Column(length = 63)
	private String firstname;
	@Column(length = 63)
	private String middlename;

	@Column(length = 63)
	private String lastname;

	@Column(length = 63)
	private String nickname;

	@JsonIgnore
	@Column(length = 63)
	private String password;

	private boolean isDisabled = false;

	//private boolean isDeleted = false;

	private boolean isVIP = false;

	private int score = 0;

	private String favicon;

	private String signature;

	private boolean isMale = true;

	private Date dob;

	@Column(length = 63)
	private String phone;

	@Column(length = 63)
	private String mobile;

	@Column(length = 63)
	private String fax;

	@Column(length = 63)
	private String occupation;

	@Column(length = 63)
	private String company;

	private String reference;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Account account = (Account) o;
		return this.id == account.getId() || this.email.equals(account.getEmail());
	}

	@Override
	public int hashCode() {
		return this.email.toLowerCase().hashCode();
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", email=" + email + ", firstname=" + firstname + ", middlename=" + middlename + ", lastname=" + lastname + ", nickname=" + nickname + ", isDisabled=" + isDisabled + "]";
	}
}
