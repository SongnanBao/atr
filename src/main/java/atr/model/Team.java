package atr.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "t_team")
@DynamicUpdate(value=true)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Team {

	public Team() {

	}

	public Team(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	@Id
	@GeneratedValue
	private Long id;

	@NotNull
	private String name;

	@OneToMany(mappedBy = "team", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonIgnore
	private List<ATR> atrs = new ArrayList<ATR>();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="parent_id")
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
	@JsonIdentityReference(alwaysAsId = true)
	private Team parent = null;

	@OneToMany(mappedBy="parent", fetch = FetchType.LAZY)
	@OrderBy
	@JsonIgnore
	private List<Team> children = new ArrayList<Team>();

	private String description = "";

	private boolean isDisabled = false;

	private boolean isRemoved = false;

	@Column(length = 63)
	private String passcode;

	@Column(length = 63)
	private String expiryDate;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Team team = (Team) o;
		return this.id == team.getId();
	}

	@Override
	public String toString() {
		return "Team [id=" + id + ", name=" + name + ", parent=" + parent + ", description=" + description + ", isDisabled=" + isDisabled + ", isRemoved=" + isRemoved + ", expiryDate=" + expiryDate + "]";
	}
}
