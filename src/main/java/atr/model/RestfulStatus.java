package atr.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Component
public class RestfulStatus<T> implements Serializable {

	private static final long serialVersionUID = -8716158173610301732L;
	private boolean success;
	private T data;
	private Map<String, List<String>> errorsList;
	private String msgCode;

	public static final String CODE_OK = "OK";
	// Common
	public static final String CODE_FAILED = "FAILED";
	public static final String CODE_NOT_AUTHENTICATED = "UNABLE_TO_AUTHENTICATE_REQUEST";
	public static final String CODE_ACCESS_DENIED = "ACCESS_DENIED";
	public static final String CODE_UNKNOWN_ERROR = "UNKNOWN_ERROR";
	public static final String CODE_ARGS_NOT_FOUND = "ARGS_NOT_FOUND";
	public static final String CODE_ARGS_ERROR = "ARGS_ERROR";
	public static final String CODE_SQL_EXCEPTION = "SQL_EXCEPTION";
	// public static final String CODE_ADMIN_NOT_FOUND = "CODE_ADMIN_NOT_FOUND";
	// Account
	public static final String CODE_SAVE_ACCOUNT_FAILED = "SAVE_ACCOUNT_FAILED";
	public static final String CODE_ACCOUNT_NOT_FOUND = "ACCOUNT_NOT_FOUND";
	// Course, Objectives, Enrol...
	public static final String CODE_COURSE_NOT_FOUND = "COURSE_NOT_FOUND";
	// team
	public static final String CODE_TEAM_NOT_FOUND = "TEAM_NOT_FOUND";
	public static final String CODE_AMBIGUOUS_TEAM = "AMBIGOUS_TEAM";
	public static final String CODE_INVALID_TEAM_ID = "INVALID_TEAM_ID";

	/**
	 * Construct a status that defaults to failed.
	 */
	public RestfulStatus() {
		this(false, null);
	}

	/**
	 * Construct a status with no errors and no data.
	 */
	public RestfulStatus(final boolean success, final String msgCode) {
		this(success, msgCode, null);
	}

	/**
	 * Construct a status with no errors.
	 */
	public RestfulStatus(final boolean success, final String msgCode, final T data) {
		this(success, msgCode, data, new HashMap<String, List<String>>());
	}

	public RestfulStatus(final boolean success, final String msgCode, final T data, final Map<String, List<String>> errors) {
		this.success = success;
		this.data = data;
		if (errors != null) {
			this.errorsList = errors;
		} else {
			this.errorsList = new HashMap<String, List<String>>();
		}
		this.msgCode = msgCode;
	}

	/**
	 * Create a failed status with the given error list.
	 */
	public RestfulStatus(final String msgCode, final T data, final Map<String, List<String>> errorsList) {
		this.success = false;
		this.data = data;
		this.msgCode = msgCode;

		if (errorsList != null) {
			this.errorsList = errorsList;
		} else {
			this.errorsList = new HashMap<String, List<String>>();
		}
	}

	public boolean isSuccess() {
		return this.success;
	}

	public void setSuccess(final boolean success) {
		this.success = success;
		if (success && StringUtils.isBlank(this.msgCode)) {
			this.msgCode = CODE_OK;
		}
	}

	public String getMsgCode() {
		return this.msgCode;
	}

	public void setMessage(final String msgCode) {
		this.msgCode = msgCode;
	}

	public T getData() {
		return this.data;
	}

	public void setData(final T data) {
		this.data = data;
	}

	@JsonIgnore
	public Map<String, List<String>> getErrors() {
		return this.errorsList;
	}

	/**
	 * Add all of the errors into the ErrorList
	 */
	public void setErrors(final Map<String, String> errors) {
		if (errors != null) {
			for (final Map.Entry<String, String> error : errors.entrySet()) {
				addError(error.getKey(), error.getValue());
			}
		}
	}

	public RestfulStatus<T> addError(final String key, final String message) {
		if (!this.errorsList.containsKey(key)) {
			this.errorsList.put(key, new ArrayList<String>());
		}
		this.errorsList.get(key).add(message);
		return this;
	}

	public Map<String, List<String>> getErrorsList() {
		return this.errorsList;
	}

	public void setErrorsList(final Map<String, List<String>> errorsList) {
		this.errorsList = errorsList;
	}

	public String[] toResultMessageArray() {
		final List<String> results = new ArrayList<String>();
		if (getErrorsList() != null && !getErrorsList().isEmpty()) {
			for (final Entry<String, List<String>> entry : getErrorsList().entrySet()) {
				for (final String err : entry.getValue()) {
					results.add(err);
				}
			}
		}
		return results.toArray(new String[] {});

	}

	public String toResultMessageFormattedString() {
		final StringBuilder result = new StringBuilder();
		if (getErrorsList() != null && !getErrorsList().isEmpty()) {
			for (final Entry<String, List<String>> entry : getErrorsList().entrySet()) {
				if (result.length() > 0) {
					result.append(", ");
				}
				result.append(entry.getKey()).append(": ").append(entry.getValue());
			}
		}
		return result.toString();
	}

}