package atr.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import atr.dto.AtrDTO;
import atr.util.json.Views;
import lombok.Data;

@Data
@Entity
@Table(name = "t_account_team_role", uniqueConstraints = @UniqueConstraint(columnNames = {
		"account_id", "team_id", "role_id"
}))
@DynamicUpdate(value = true)
public class ATR {

	@Id
	@GeneratedValue
	@JsonView(Views.BasicView.class)
	private Long id;

	@ManyToOne(cascade = {
			CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH
	}, fetch = FetchType.LAZY)
	@NotNull
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
	@JsonIdentityReference(alwaysAsId = true)
	@JsonView(Views.BasicView.class)
	private Account account;

	@ManyToOne(cascade = {
			CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH
	}, fetch = FetchType.LAZY)
	@NotNull
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
	@JsonIdentityReference(alwaysAsId = true)
	@JsonView(Views.BasicView.class)
	private Team team;

	@ManyToOne(cascade = {
			CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH
	}, fetch = FetchType.LAZY)
	@NotNull
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
	@JsonIdentityReference(alwaysAsId = true)
	@JsonView(Views.BasicView.class)
	private Role role;

	public ATR() {
	}

	public ATR(Account account, Team team, Role role) {
		super();
		this.account = account;
		this.team = team;
		this.role = role;
	}

	public AtrDTO toDTO() {
		return new AtrDTO(id, account.getId(), team.getId(), role.getId());
	}
}
