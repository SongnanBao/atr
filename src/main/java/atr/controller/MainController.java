package atr.controller;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@EnableAutoConfiguration
public class MainController {

	@RequestMapping("/")
	@ResponseBody
	public String index() {
		return "Proudly handcrafted by '_tech_' :)";
	}

}
