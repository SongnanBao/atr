package atr.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import atr.exception.RestfulException;
import atr.model.Account;
import atr.model.Role;
import atr.model.Team;
import atr.service.TeamService;
import atr.util.EntityUtil;

@Controller
@RequestMapping("/api/team")
public class TeamController {

	@Autowired
	private TeamService teamService;

	@RequestMapping(value = "/", method = RequestMethod.POST)
	@ResponseBody
	public Team create(@RequestParam("name") String name, @RequestParam("desciption") String desciption) {
		return teamService.create(name, desciption);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Team get(@PathVariable long id) {
		return teamService.get(id);
	}

	@RequestMapping(value = "/byname/", method = RequestMethod.GET)
	@ResponseBody
	public List<Team> getByName(@RequestParam(value="name", required=true) String name) {
		return teamService.getByName(name);
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	@ResponseBody
	public List<Team> getAll() {
		return teamService.getAll();
	}

	@RequestMapping(value = "/", method = RequestMethod.PATCH)
	@ResponseBody
	public Team update(@RequestBody Team team) {
		if (team == null || team.getId() == null || team.getId() <= 0) {
			throw new RestfulException("Invalid Team.", HttpStatus.UNPROCESSABLE_ENTITY.value());
		}
		Team existingTeam = teamService.get(team.getId());
		EntityUtil.copyNonNullProperties(team, existingTeam);
		return teamService.update(existingTeam);
	}

	@RequestMapping(value = "/{id}/parent", method = RequestMethod.GET)
	@ResponseBody
	public Team getParent(@PathVariable long id) {
		return teamService.getParent(id);
	}

	@RequestMapping(value = "/{id}/children", method = RequestMethod.GET)
	@ResponseBody
	public List<Team> getChildren(@PathVariable long id) {
		return teamService.getChildren(id);
	}

	@RequestMapping(value = "/{id}/accounts", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<Account>> getAccounts(@PathVariable long id) {
		List<Account> accounts = teamService.getAccounts(id);
		return new ResponseEntity<List<Account>>(accounts, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}/roles", method = RequestMethod.GET)
	@ResponseBody
	public List<Role> getRoles(@PathVariable long id) {
		return teamService.getRoles(id);
	}

	@RequestMapping(value = "/{id}/descendants", method = RequestMethod.GET)
	@ResponseBody
	public List<Team> getDescendants(@PathVariable long id) {
		return teamService.getDescendants(id);
	}

	@RequestMapping(value = "/{id}/descendantids", method = RequestMethod.GET)
	@ResponseBody
	public List<Long> getDescendantIds(@PathVariable long id) {
		return teamService.getDescendantIds(id);
	}

	@RequestMapping(value = "/{id}/ancestors", method = RequestMethod.GET)
	@ResponseBody
	public List<Team> getAncestors(@PathVariable long id) {
		return teamService.getAncestors(id);
	}

	@RequestMapping(value = "/{id}/ancestorids", method = RequestMethod.GET)
	@ResponseBody
	public List<Long> getAncestorIds(@PathVariable long id) {
		return teamService.getAncestorIds(id);
	}

	@RequestMapping(value = "/{id}/parent/{parentId}", method = RequestMethod.PUT)
	@ResponseBody
	public Team setParent(@PathVariable("id") long id, @PathVariable("parentId") long parentId) {
		return teamService.setParent(id, parentId);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public String delete(@PathVariable("id") long id) {
		return teamService.delete(id);
	}

	@RequestMapping(value = "/{id}/restore", method = RequestMethod.POST)
	@ResponseBody
	public String restore(@PathVariable("id") long id) {
		return teamService.restore(id);
	}

	@RequestMapping(value = "/{id}/disable", method = RequestMethod.POST)
	@ResponseBody
	public String disable(@PathVariable("id") long id) {
		return teamService.disable(id);
	}

	@RequestMapping(value = "/{id}/enable", method = RequestMethod.POST)
	@ResponseBody
	public String enable(@PathVariable("id") long id) {
		return teamService.enable(id);
	}

	@RequestMapping(value = "/{id}/parent/{parentId}", method = RequestMethod.DELETE)
	@ResponseBody
	public String removeParent(@PathVariable("id") long id, @PathVariable("parentId") long parentId) {
		return teamService.removeParent(id, parentId);
	}
}
