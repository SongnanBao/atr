package atr.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import atr.exception.RestfulException;
import atr.model.Account;
import atr.model.Role;
import atr.model.Team;
import atr.service.AccountService;
import atr.util.EntityUtil;

@Controller
@RequestMapping("/api/account")
public class AccountController {

	@Autowired
	private AccountService accountService;

	@RequestMapping(value = "/", method = RequestMethod.POST)
	@ResponseBody
	public Account create(
			@RequestParam("email") String email,
			@RequestParam("firstname") String firstname,
			@RequestParam("lastname") String lastname,
			@RequestParam("password") String password) {
		if (accountService.getByEmail(email) != null) {
			throw new RestfulException("Existing Account with Email '"+email+"'. ", HttpStatus.UNPROCESSABLE_ENTITY.value());
		}
		return accountService.create(email, firstname, lastname, password);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Account get(@PathVariable long id) {
		return accountService.get(id);
	}

	@RequestMapping(value = "/byemail/", method = RequestMethod.GET)
	@ResponseBody
	public Account getByEmail(@RequestParam(value="email", required=true) String email) {
		return accountService.getByEmail(email);
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	@ResponseBody
	public List<Account> getAll() {
		return accountService.getAll();
	}

	@RequestMapping(value = "/{id}/teams", method = RequestMethod.GET)
	@ResponseBody
	public List<Team> getTeams(@PathVariable long id) {
		return accountService.getTeams(id);
	}

	@RequestMapping(value = "/{id}/roles", method = RequestMethod.GET)
	@ResponseBody
	public List<Role> getRoles(@PathVariable long id) {
		return accountService.getRoles(id);
	}

	@RequestMapping(value = "/", method = RequestMethod.PATCH)
	@ResponseBody
	public Account update(@RequestBody Account account) {
		if (account == null || account.getId() == null || account.getId() <= 0) {
			throw new RestfulException("Invalid Account.", HttpStatus.UNPROCESSABLE_ENTITY.value());
		}
		Account existingAccount = accountService.get(account.getId());
		EntityUtil.copyNonNullProperties(account, existingAccount);
		return accountService.save(existingAccount);
	}

	/*
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public String delete(@PathVariable("id") long id) {
		return accountService.delete(id);
	}
	 */

	@RequestMapping(value = "/{id}/disable", method = RequestMethod.POST)
	@ResponseBody
	public String disable(@PathVariable("id") long id) {
		return accountService.disable(id);
	}

	@RequestMapping(value = "/{id}/enable", method = RequestMethod.POST)
	@ResponseBody
	public String enable(@PathVariable("id") long id) {
		return accountService.enable(id);
	}
}
