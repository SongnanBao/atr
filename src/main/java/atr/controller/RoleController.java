package atr.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import atr.exception.RestfulException;
import atr.model.Account;
import atr.model.Role;
import atr.model.Team;
import atr.service.RoleService;
import atr.util.EntityUtil;

@Controller
@RequestMapping("/api/role")
public class RoleController {

	@Autowired
	private RoleService roleService;

	@RequestMapping(value = "/", method = RequestMethod.POST)
	@ResponseBody
	public Role create(@RequestParam("name") String name, @RequestParam("description") String description) {
		return roleService.create(name, description);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Role get(@PathVariable long id) {
		return roleService.get(id);
	}

	@RequestMapping(value = "/byname/{name}", method = RequestMethod.GET)
	@ResponseBody
	public Role getByType(@PathVariable String name) {
		return roleService.getByName(name);
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	@ResponseBody
	public List<Role> getAll() {
		return roleService.getAll();
	}

	@RequestMapping(value = "/", method = RequestMethod.PATCH)
	@ResponseBody
	public Role update(@RequestBody Role role) {
		if (role == null || role.getId() == null || role.getId() <= 0) {
			throw new RestfulException("Invalid Role.", HttpStatus.UNPROCESSABLE_ENTITY.value());
		}
		Role existingRole = roleService.get(role.getId());
		EntityUtil.copyNonNullProperties(role, existingRole);
		return roleService.update(role);
	}

	@RequestMapping(value = "/{id}/accounts", method = RequestMethod.GET)
	@ResponseBody
	public List<Account> getAccounts(@PathVariable long id) {
		return roleService.getAccounts(id);
	}

	@RequestMapping(value = "/{id}/teams", method = RequestMethod.GET)
	@ResponseBody
	public List<Team> getTeams(@PathVariable long id) {
		return roleService.getTeams(id);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public String delete(@PathVariable("id") long id) {
		return roleService.delete(id);
	}
}
