package atr.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import atr.dto.AtrDTO;
import atr.service.ATRService;

@Controller
@RequestMapping("/api/atr")
public class ATRController {

	@Autowired
	private ATRService atrService;

	@RequestMapping(value = "/", method = RequestMethod.POST)
	@ResponseBody
	public AtrDTO create(@RequestParam("accountId") long accountId,
			@RequestParam("teamId") long teamId,
			@RequestParam("roleId") long roleId) throws Exception {
		return atrService.createAtr(accountId, teamId, roleId);
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	@ResponseBody
	public List<AtrDTO> getAll() throws Exception {
		return atrService.getAllAtrDTOs();
	}

	@RequestMapping(value = "/{accountId}/{teamId}/{roleId}", method = RequestMethod.GET)
	@ResponseBody
	public AtrDTO get(@PathVariable("accountId") long accountId,
			@PathVariable("teamId") long teamId,
			@PathVariable("roleId") long roleId) throws Exception {
		return atrService.getAtrDTO(accountId, teamId, roleId);
	}

	@RequestMapping(value = "/account/{accountId}", method = RequestMethod.GET)
	@ResponseBody
	public List<AtrDTO> getByAccountId(@PathVariable("accountId") long accountId) throws Exception {
		return atrService.getAtrsByAccount(accountId);
	}

	@RequestMapping(value = "/team/{teamId}", method = RequestMethod.GET)
	@ResponseBody
	public List<AtrDTO> getByTeamId(@PathVariable("teamId") long teamId) throws Exception {
		return atrService.getAtrsByTeam(teamId);
	}

	@RequestMapping(value = "/role/{roleId}", method = RequestMethod.GET)
	@ResponseBody
	public List<AtrDTO> getByRoleId(@PathVariable("roleId") long roleId) throws Exception {
		return atrService.getAtrsByRole(roleId);
	}

	@RequestMapping(value = "/account/{accountId}/team/{teamId}", method = RequestMethod.GET)
	@ResponseBody
	public List<AtrDTO> getByAccountIdAndTeamId(@PathVariable("accountId") long accountId, @PathVariable("teamId") long teamId) throws Exception {
		return atrService.getAtrsByAccountAndTeam(accountId, teamId);
	}

	@RequestMapping(value = "/account/{accountId}/role/{roleId}", method = RequestMethod.GET)
	@ResponseBody
	public List<AtrDTO> getByAccountIdAndRoleId(@PathVariable("accountId") long accountId, @PathVariable("roleId") long roleId) throws Exception {
		return atrService.getAtrsByAccountAndRole(accountId, roleId);
	}

	@RequestMapping(value = "/team/{teamId}/role/{roleId}", method = RequestMethod.GET)
	@ResponseBody
	public List<AtrDTO> getByTeamIdAndRoleId(@PathVariable("teamId") long teamId, @PathVariable("roleId") long roleId) throws Exception {
		return atrService.getAtrsByTeamAndRole(teamId, roleId);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public void remove(@PathVariable("id") long id) throws Exception {
		atrService.removeAtr(id);
	}

	@RequestMapping(value = "/account/{accountId}/team/{teamId}/role/{roleId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void remove(@PathVariable("accountId") long accountId,
			@PathVariable("teamId") long teamId,
			@PathVariable("roleId") long roleId) throws Exception {
		atrService.removeAtr(accountId, teamId, roleId);
	}

	@RequestMapping(value = "/account/{accountId}/team/{teamId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void removeByAccountAndTeam(@PathVariable("accountId") long accountId, @PathVariable("teamId") long teamId) throws Exception {
		atrService.removeAtrsByAccountAndTeam(accountId, teamId);
	}

	@RequestMapping(value = "/account/{accountId}/role/{roleId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void removeByAccountAndRole(@PathVariable("accountId") long accountId, @PathVariable("roleId") long roleId) throws Exception {
		atrService.removeAtrsByAccountAndRole(accountId, roleId);
	}

	@RequestMapping(value = "/team/{teamId}/role/{roleId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void removeByTeamAndRole(@PathVariable("teamId") long teamId, @PathVariable("roleId") long roleId) throws Exception {
		atrService.removeAtrsByTeamAndRole(teamId, roleId);
	}

	@RequestMapping(value = "/account/{accountId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void removeByAccount(@PathVariable("accountId") long accountId) throws Exception {
		atrService.removeAtrsByAccount(accountId);
	}

	@RequestMapping(value = "/team/{teamId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void removeByTeam(@PathVariable("teamId") long teamId) throws Exception {
		atrService.removeAtrsByTeam(teamId);
	}

	@RequestMapping(value = "/role/{roleId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void removeByRole(@PathVariable("roleId") long roleId) throws Exception {
		atrService.removeAtrsByRole(roleId);
	}
}
