package atr.util;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import atr.dto.AtrDTO;
import atr.model.Account;
import atr.model.Role;
import atr.model.Team;
import atr.repository.ATRRepository;
import atr.service.ATRService;
import atr.service.AccountService;
import atr.service.RoleService;
import atr.service.TeamService;

@Component
@RunWith(SpringRunner.class)
@SpringBootTest()
public class AuthorityUtilTest {

	@Autowired
	private AccountService accountService;
	@Autowired
	private TeamService teamService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private ATRService atrService;
	@Autowired
	private ATRRepository atrRepository;

	@Test
	public void testIsSuperManagerAccount() {
		AuthorityUtil util = new AuthorityUtil();
		Account superAccount = accountService.getByEmail("super@atr.com");
		if (superAccount == null) {
			superAccount = accountService.create("super@atr.com", "super", "atr", "password");
		}
		Team team = null;
		List<Team> teams = teamService.getByName("superTeam");
		if (teams == null || teams.size()==0) {
			team = teamService.create("superTeam", "");
		} else {
			team = teams.get(0);
		}
		Role role = roleService.getByName("super");
		if (role == null) {
			role = roleService.create("superTeam", "");
		}
		try {
			AtrDTO atrDTO = atrService.getAtrDTO(superAccount.getId(), team.getId(), role.getId());
			if (atrDTO == null) {
				atrDTO = atrService.createAtr(superAccount.getId(), team.getId(), role.getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		assertTrue(util.isSuperManager(superAccount));
	}

	@Test
	public void testIsSuperManagerATR() {
		fail("Not yet implemented");
	}

	@Test
	public void testCanManageATRATR() {
		fail("Not yet implemented");
	}

	@Test
	public void testCanManageAccountAccount() {
		fail("Not yet implemented");
	}

	@Test
	public void testCanManageAccountTeam() {
		fail("Not yet implemented");
	}

	@Test
	public void testCanAccessATRATR() {
		fail("Not yet implemented");
	}

	@Test
	public void testCanAccessAccountAccount() {
		fail("Not yet implemented");
	}

	@Test
	public void testCanAccessAccountTeam() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetManagers() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetManagersAccount() {
		fail("Not yet implemented");
	}

	@Test
	public void testIsManager() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetAccountsManagedBy() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTeamsManagedBy() {
		fail("Not yet implemented");
	}

}
