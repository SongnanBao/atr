package atr.util;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import atr.dto.AtrDTO;
import atr.model.ATR;
import atr.model.Account;
import atr.model.Role;
import atr.model.Team;
import atr.service.ATRService;
import atr.service.AccountService;
import atr.service.RoleService;
import atr.service.TeamService;

@Component
public class AuthorityUtil {

	public static final String ADMIN = "Admin";

	public static final String SUPER = "Super";

	private static final Logger LOG = LoggerFactory.getLogger(AuthorityUtil.class);

	@Autowired
	private AccountService accountService;
	@Autowired
	private TeamService teamService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private ATRService atrService;

	public boolean isSuperManager(Account account) {
		List<ATR> atrs = account.getAtrs();
		for (ATR atr : atrs) {
			if (isSuperManager(atr)) {
				return true;
			}
		}
		return false;
	}

	public boolean isSuperManager(ATR atr) {
		Role role = atr.getRole();
		return role.isManager() && SUPER.equalsIgnoreCase(role.getName());
	}

	public boolean canManage(ATR atr, ATR objectAtr) {
		if (isSuperManager(atr)) {
			return true;
		}
		Role role = atr.getRole();
		Role objectRole = objectAtr.getRole();
		if (role.isManager() && role.getLevel() > objectRole.getLevel()) {
			Team team = atr.getTeam();
			Team objectTeam = objectAtr.getTeam();
			if (team.equals(objectTeam) || teamService.isAncestor(team.getId(), objectTeam.getId())) {
				return true;
			}
		}
		return role.getName().equalsIgnoreCase(SUPER);
	}

	public boolean canManage(Account account, Account objectAccount) {
		if (isSuperManager(account)) {
			return true;
		}
		List<ATR> atrs = account.getAtrs();
		List<ATR> objectAtrs = objectAccount.getAtrs();
		for (ATR atr : atrs) {
			for (ATR objectAtr : objectAtrs) {
				if (canManage(atr, objectAtr)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean canManage(Account account, Team objectTeam) {
		if (isSuperManager(account)) {
			return true;
		}
		try {
			List<AtrDTO> atrs = atrService.getAtrsByAccount(account.getId());
			for (AtrDTO atrDTO : atrs) {
				Long teamId = atrDTO.getTeamId();
				Role role = roleService.get(atrDTO.getRoleId());
				if (role != null) {
					if (role.isManager() && role.getName().equalsIgnoreCase(ADMIN)) {
						if (teamId.longValue() == objectTeam.getId().longValue() || teamService.isAncestor(teamId, objectTeam.getId())) {
							return true;
						}
					}
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return false;
	}

	public boolean canAccess(ATR atr, ATR objectAtr) {
		Team team = atr.getTeam();
		Team objectTeam = objectAtr.getTeam();
		if (canManage(atr, objectAtr) || team.equals(objectTeam)) {
			return true;
		}
		return false;
	}

	public boolean canAccess(Account account, Account objectAccount) {
		List<ATR> atrs = account.getAtrs();
		List<ATR> objectAtrs = objectAccount.getAtrs();
		for (ATR atr : atrs) {
			for (ATR objectAtr : objectAtrs) {
				if (canAccess(atr, objectAtr)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean canAccess(Account account, Team objectTeam) {
		try {
			List<AtrDTO> atrs = atrService.getAtrsByAccount(account.getId());
			for (AtrDTO atr : atrs) {
				Long teamId = atr.getTeamId();
				if (canManage(account, objectTeam) || teamId.longValue() == objectTeam.getId().longValue()) {
					return true;
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return false;
	}

	public List<Account> getManagers() {
		List<Account> managers = new ArrayList<Account>();
		List<Account> accounts = accountService.getAll();
		for (Account account : accounts) {
			if (isManager(account)) {
				managers.add(account);
			}
		}
		return managers;
	}

	public List<Account> getManagers(Account account) {
		List<Account> accountManagers = new ArrayList<Account>();
		List<Account> allManagers = getManagers();
		for (Account manager : allManagers) {
			if (canManage(manager, account)) {
				accountManagers.add(account);
			}
		}
		return accountManagers;
	}

	public boolean isManager(Account account) {
		List<ATR> atrs = account.getAtrs();
		for (ATR atr : atrs) {
			if (atr.getRole().isManager()) {
				return true;
			}
		}
		return false;
	}

	public List<Account> getAccountsManagedBy(Account manager) {
		List<Account> accountsManagedBy = new ArrayList<Account>();
		List<Account> allAccounts = accountService.getAll();
		for (Account account : allAccounts) {
			if (canManage(manager, account)) {
				accountsManagedBy.add(account);
			}
		}
		return accountsManagedBy;
	}

	public List<Team> getTeamsManagedBy(Account manager) {
		List<Team> teamsManagedBy = new ArrayList<Team>();
		List<Team> allTeams = teamService.getAll();
		for (Team team : allTeams) {
			if (canManage(manager, team)) {
				teamsManagedBy.add(team);
			}
		}
		return teamsManagedBy;
	}

}
