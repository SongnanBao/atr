package atr.util.json;

public class Views {
	public interface BasicView {}
	public interface PublicView extends BasicView {}
	public interface PrivateView extends BasicView {}
}
