package atr.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import atr.model.Team;

@Transactional
public interface TeamRepository extends JpaRepository<Team, Long> {

	public List<Team> findByName(String name);

}
