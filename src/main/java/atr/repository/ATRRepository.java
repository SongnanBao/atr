package atr.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import atr.model.ATR;

@Transactional
public interface ATRRepository extends JpaRepository<ATR, Long> {

	public ATR findByAccountIdAndTeamIdAndRoleId(Long accountId, Long teamId, Long roleId);

	public List<ATR> findByAccountId(Long accountId);
	public List<ATR> findByTeamId(Long teamId);
	public List<ATR> findByRoleId(Long roleId);

	public List<ATR> findByAccountIdAndTeamId(Long accountId, Long teamId);
	public List<ATR> findByAccountIdAndRoleId(Long accountId, Long roleId);
	public List<ATR> findByTeamIdAndRoleId(Long teamId, Long roleId);

	public void deleteById(Long id);
	public void deleteByAccountId(Long accountId);
	public void deleteByTeamId(Long teamId);
	public void deleteByRoleId(Long roleId);

	public void deleteByAccountIdAndTeamId(Long accountId, Long teamId);
	public void deleteByAccountIdAndRoleId(Long accountId, Long roleId);
	public void deleteByTeamIdAndRoleId(Long teamId, Long roleId);

	/*
	@Modifying
	@Query(value = "DELETE FROM t_account_team_role WHERE account_id = ?1", nativeQuery = true)
	void deleteByAccount(Long accountId);

	@Modifying
	@Query(value = "DELETE FROM t_account_team_role WHERE team_id = ?1", nativeQuery = true)
	void deleteByTeam(Long teamId);

	@Modifying
	@Query(value = "DELETE FROM t_account_team_role WHERE role_id = ?1", nativeQuery = true)
	void deleteByRole(Long roleId);


	@Modifying
	@Query(value = "DELETE FROM t_account_team_role WHERE account_id = ?1 AND team_id = ?2", nativeQuery = true)
	void deleteByAccountAndTeam(Long accountId, Long teamId);

	@Modifying
	@Query(value = "DELETE FROM t_account_team_role WHERE account_id = ?1 AND role_id = ?2", nativeQuery = true)
	void deleteByAccountAndRole(Long accountId, Long roleId);

	@Modifying
	@Query(value = "DELETE FROM t_account_team_role WHERE team_id = ?1 AND role_id = ?2", nativeQuery = true)
	void deleteByTeamAndRole(Long teamId, Long roleId);


	@Modifying
	@Query(value = "DELETE FROM t_account_team_role WHERE account_id = ?1 AND team_id = ?2 AND role_id = ?3", nativeQuery = true)
	void deleteByAccountAndTeamAndRole(Long accountId, Long teamId, Long roleId);
	 */


	/*@Query(value = "SELECT id, account_id, team_id, role_id FROM t_account_team_role WHERE id = ?1 LIMIT 1", nativeQuery = true)
	Object findById(Long atrId);

	@Override
	@Query(value = "SELECT atr FROM ATR atr WHERE atr.id = ?1 ")
	public ATR findOne(Long id);


	@Query(value = "SELECT id, account_id, team_id, role_id FROM t_account_team_role WHERE account_id = ?1 AND team_id = ?2 AND role_id = ?3 LIMIT 1", nativeQuery = true)
	Object findByAccountAndTeamAndRole(Long accountId, Long teamId, Long roleId);


	@Query(value = "SELECT atr FROM ATR atr WHERE atr.account.id = ?1 AND atr.team.id = ?2 AND atr.role.id = ?3 ")
	public ATR findByAccountAndTeamAndRole(Long accountId, Long teamId, Long roleId);

	@Query(value = "SELECT id, account_id, team_id, role_id FROM t_account_team_role WHERE account_id = ?1", nativeQuery = true)
	List<Object> findByAccount(Long accountId);

	@Query(value = "SELECT atr FROM ATR atr WHERE atr.account.id = ?1 ")
	public List<ATR> findByAccount(Long accountId);

	@Query(value = "SELECT id, account_id, team_id, role_id FROM t_account_team_role WHERE team_id = ?1", nativeQuery = true)
	List<Object> findByTeam(Long teamId);
	@Query(value = "SELECT atr FROM ATR atr WHERE atr.team.id = ?1 ")
	public List<ATR> findByTeam(Long teamId);

	@Query(value = "SELECT id, account_id, team_id, role_id FROM t_account_team_role WHERE role_id = ?1", nativeQuery = true)
	List<Object> findByRole(Long roleId);
	@Query(value = "SELECT atr FROM ATR atr WHERE atr.role.id = ?1 ")
	public List<ATR> findByRole(Long roleId);

	@Query(value = "SELECT id, account_id, team_id, role_id FROM t_account_team_role", nativeQuery = true)
	List<Object> findAllAtrs();
	@Query(value = "SELECT atr FROM ATR atr ")
	public List<ATR> findAll();

	@Query(value = "SELECT id, account_id, team_id, role_id FROM t_account_team_role WHERE account_id = ?1 AND team_id = ?2", nativeQuery = true)
	List<Object> findByAccountAndTeam(Long accountId, Long teamId);
	@Query(value = "FROM ATR atr WHERE atr.account.id = ?1 AND atr.team.id = ?2")
	public List<ATR> findByAccountAndTeam(Long accountId, Long teamId);

	@Query(value = "SELECT id, account_id, team_id, role_id FROM t_account_team_role WHERE account_id = ?1 AND role_id = ?2", nativeQuery = true)
	List<Object> findByAccountAndRole(Long accountId, Long roleId);
	@Query(value = "SELECT atr FROM ATR atr WHERE atr.account.id = ?1 AND atr.role.id = ?2")
	public List<ATR> findByAccountAndRole(Long accountId, Long roleId);

	@Query(value = "SELECT id, account_id, team_id, role_id FROM t_account_team_role WHERE team_id = ?1 AND role_id = ?2", nativeQuery = true)
	List<Object> findByTeamAndRole(Long teamId, Long roleId);
	@Query(value = "SELECT atr FROM ATR atr WHERE atr.team.id = ?1 AND atr.role.id = ?2")
	public List<ATR> findByTeamAndRole(Long teamId, Long roleId);
	 */
}
