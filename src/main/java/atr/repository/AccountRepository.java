package atr.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import atr.model.Account;

@Transactional
public interface AccountRepository extends JpaRepository<Account, Long> {

	public Account findByEmail(String email);
	public void deleteByEmail(String email);
	public List<Account> findAllByFirstname();
	public List<Account> findAllByLastname();

}
