package atr.exception;

import atr.model.RestfulStatus;

public class RestfulException extends RuntimeException {

	private static final long serialVersionUID = -5651304438263939611L;

	private final int errorCode;
	private RestfulStatus<?> status;

	public RestfulException(int errorCode) {
		super();
		this.errorCode = errorCode;
	}

	public RestfulException(String message, int errorCode) {
		super(message);
		this.errorCode = errorCode;
	}

	public RestfulException(String message, Throwable cause, int errorCode) {
		super(message, cause);
		this.errorCode = errorCode;
	}

	public RestfulException(Throwable cause, int errorCode) {
		super(cause);
		this.errorCode = errorCode;
	}

	public RestfulException(int errorCode, RestfulStatus<?> status) {
		this.status = status;
		this.errorCode = errorCode;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public RestfulStatus<?> getStatus() {
		return this.status;
	}

}