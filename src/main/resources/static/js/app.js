var app = angular.module('app', ['ngRoute','ngResource']);
app.config(function($routeProvider){
    $routeProvider
        .when('/accounts',{
            templateUrl: '/views/accounts.html',
            controller: 'accountsController'
        })
        .when('/account/',{
            templateUrl: '/views/account.html',
            controller: 'accountController'
        })
        .when('/roles',{
            templateUrl: '/views/roles.html',
            controller: 'rolesController'
        })
        .when('/teams',{
            templateUrl: '/views/teams.html',
            controller: 'teamsController'
        })
        .otherwise(
            { redirectTo: '/'}
        );
});

