app.controller('accountsController', function($scope, $http) {
    $scope.headingTitle = "Accounts List";
    $http.get('http://localhost:8091/api/account/').
        then(function(response) {
            $scope.accounts = response.data;
        });
});
app.controller('accountController', function($scope, $http) {
    $scope.headingTitle = "Account";
    $http.get('http://localhost:8091/api/account/').
        then(function(response) {
            $scope.accounts = response.data;
        });
});

app.controller('teamsController', function($scope, $http) {
    $scope.headingTitle = "Teams List";
    $http.get('http://localhost:8091/api/team/').
        then(function(response) {
            $scope.teams = response.data;
        });
});

app.controller('rolesController', function($scope, $http) {
    $scope.headingTitle = "Roles List";
    $http.get('http://localhost:8091/api/role/').
        then(function(response) {
            $scope.roles = response.data;
        });
});