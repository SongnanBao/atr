package atr.repository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ATRRepositoryTest {

	@Autowired
	ATRRepository atrRepository;

	@Test
	public void testFindById() {
		Assert.assertNotNull(atrRepository.findById(20L));
	}

	@Test
	public void testFindByAccountIdAndTeamIdAndRoleId() {
		Assert.assertNotNull(atrRepository.findByAccountIdAndTeamIdAndRoleId(1L, 4L, 8L));
	}

	@Test
	public void testFindByAccountId() {
		Assert.assertNotNull(atrRepository.findByAccountId(1L));
	}

	@Test
	public void testFindByTeamId() {
		Assert.assertNotNull(atrRepository.findByTeamId(4L));
	}

	@Test
	public void testFindByRoleId() {
		Assert.assertNotNull(atrRepository.findByRoleId(8L));
	}

	@Test
	public void testFindByAccountIdAndTeamId() {
		Assert.assertNotNull(atrRepository.findByAccountIdAndTeamId(1L, 4L));
	}

	@Test
	public void testFindByAccountIdAndRoleId() {
		Assert.assertNotNull(atrRepository.findByAccountIdAndRoleId(1L, 8L));
	}

	@Test
	public void testFindByTeamIdAndRoleId() {
		Assert.assertNotNull(atrRepository.findByTeamIdAndRoleId(4L, 8L));
	}

	@Test
	public void testDeleteByAccountId() {
		Assert.assertNotNull(atrRepository);
	}

	@Test
	public void testDeleteByTeamId() {
		Assert.assertNotNull(atrRepository);
	}

	@Test
	public void testDeleteByRoleId() {
		Assert.assertNotNull(atrRepository);
	}

	@Test
	public void testDeleteByAccountIdAndTeamId() {
		Assert.assertNotNull(atrRepository);
	}

	@Test
	public void testDeleteByAccountIdAndRoleId() {
		Assert.assertNotNull(atrRepository);
	}

	@Test
	public void testDeleteByTeamIdAndRoleId() {
		Assert.assertNotNull(atrRepository);
	}

	@Test
	public void testDeleteByAccountIdAndTeamIdAndRoleId() {
		Assert.assertNotNull(atrRepository);
	}
}
