package atr.repository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TeamRepositoryTest {

	@Autowired
	TeamRepository teamRepository;

	@Test
	public void testFindByName() {
		Assert.assertNotNull(teamRepository.findByName("team1"));
	}
}
