package atr.repository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountRepositoryTest {

	@Autowired
	AccountRepository accountRepository;

	@Test
	public void testFindByEmail() {
		Assert.assertNotNull(accountRepository.findByEmail("song@ozteaching.com"));
	}
	@Test
	public void testFindById() {
		Assert.assertNotNull(accountRepository.findById(1L));
	}

}
